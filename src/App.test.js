import React from 'react';
import App from './App';
import { mount, shallow } from 'enzyme';
import { shallowWithIntl, mountWithIntl } from './helpers/intl-enzyme-test-helper.js'

it('renders without crashing', () => {
  shallowWithIntl(<App />);
});

test('has title', () => {
  const wrapper = mountWithIntl(<App />);
  const title = wrapper.find('h1');
  expect(title).not.toBeNull();
});
