import React, { Component } from 'react';
import { Media, Content, Image } from 'react-bulma-components/full';
import { FormattedRelative } from "react-intl";
import logo from '../logo.png'

export class PostRow extends Component {
  render() {
    let post = this.props.post
    return(
      <Media>
        <Media.Item renderAs="figure" position="left">
          <Image size={24} alt="Logo" src={ logo } />
        </Media.Item>
        <Media.Item>
          <Content>
            <strong className="is-size-6">{ post.title }</strong>
          </Content>
        </Media.Item>
        { post.published &&
        <Media.Item position="right">
          <FormattedRelative value={ post.published }/>
        </Media.Item>
        }
      </Media>
    )
  }
}
