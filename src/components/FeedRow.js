import React, { Component } from 'react';
import { Media, Content, Image, Button } from 'react-bulma-components/full';
import logo from '../logo.png'

export class FeedRow extends Component {
  render() {
    let feed     = this.props.feed
    return(
      <Media className="FeedRow">
        <Media.Item renderAs="figure" position="left">
          { feed.icon ? (
            <Image size={24} alt="Feed logo"
              src={ feed.icon } />
          ) : (
            <Image size={24} alt="Feed logo"
              src={ logo } />
          )}
        </Media.Item>
        <Media.Item>
          <Content>
            <strong className="FeedRow">{ feed.title }</strong>
          </Content>
        </Media.Item>
        <Media.Item position='right'>
          <Button remove={true}
            onClick={() =>
                this.props.removeFeed(feed.id)
            }
          />
        </Media.Item>
      </Media>
    )
  }
}
