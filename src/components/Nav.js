import React, { Component } from 'react';
import { Navbar, Dropdown, Form, Icon } from 'react-bulma-components/full';
import { FormattedMessage, injectIntl } from "react-intl";
import logo from '../logo.png';
import gitlab_logo from '../icons/gitlab-logo.svg';
import es_flag from '../icons/es-flag.svg';
import gb_flag from '../icons/gb-flag.svg';
import settings_icon from '../icons/settings-gear-63.svg';
import search_icon from '../icons/zoom-2.svg';


export class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: ""
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    if (event.key === 'Enter') {
      let value = this.state.value
      // Maybe be a url regexp pattern
      var pattern = new RegExp('^https?://', 'i');
      // If starts as url try to add feed
      // else search by posts title
      if (pattern.test(value)) {
        this.props.addFeed(value);
        this.setState({value: ""})
      } else {
        this.props.searchPostByTitle(value)
      }
    }
  }

  handleFilterReadPosts = filterReadPosts => {
    this.setState({filterReadPosts})
    this.props.setFilterReadPosts( filterReadPosts )
  }


  render() {
    const InputIntl = injectIntl(({message, intl}) => (
      <Form.Input
        className='nav-input is-info'
        placeholder={intl.formatMessage({id:'nav.input.placeholder'})}
        type='text'
        value={this.state.value}
        onChange={ this.handleChange }
        onKeyPress={ this.handleSubmit }
        autoFocus
      />
    ));

    const SettingsIntl = injectIntl(({message, intl}) => (
      <img
        src={settings_icon}
        alt={intl.formatMessage({id:'nav.options'})}
        width="28"
        height="28"
      />
    ));

    return(

      <Navbar color='dark' fixed='top'>
        <Navbar.Brand>
          <Navbar.Item renderAs="a" href="#">
            <img
              src={logo}
              alt="rawRSS Logo"
              width="28"
              height="28"
            />
            <h1 className="nav-title">rawRSS</h1>
          </Navbar.Item>
        </Navbar.Brand>
        <Navbar.Menu>
          <Navbar.Container>
            <Navbar.Item>
              <Form.Field>
                <Form.Control iconRight>
                  <InputIntl />
                  <Icon align="right">
                    <img src={search_icon} alt="Search icon" width="20" height="20" />
                  </Icon>
                </Form.Control>
              </Form.Field>
            </Navbar.Item>
            <Navbar.Item href="#" className='has-text-info'
              onClick={this.props.refresh}>
              <FormattedMessage id='nav.refresh' />
            </Navbar.Item>
          </Navbar.Container>
          <Navbar.Container position="end">
            <Navbar.Item>
              <Dropdown hoverable color='dark'
                value={this.state.filterReadPosts}
                onChange={this.handleFilterReadPosts}>
                <Dropdown.Item value="">
                  <FormattedMessage id='nav.show.all' />
                </Dropdown.Item>
                <Dropdown.Item value="true">
                  <FormattedMessage id='nav.show.unread' />
                </Dropdown.Item>
              </Dropdown>
            </Navbar.Item>
            <Navbar.Item renderAs="a" href="/?locale=en">
              <img
                src={gb_flag}
                alt="English"
                width="20"
                height="20"
              />
            </Navbar.Item>
            <Navbar.Item renderAs="a" href="/?locale=es">
              <img
                src={es_flag}
                alt="Español"
                width="28"
                height="28"
              />
            </Navbar.Item>
            <Navbar.Item href="#">
              <SettingsIntl />
            </Navbar.Item>
            <Navbar.Item renderAs="a" href="https://gitlab.com/cravacuore/rawRSS/">
              <img
                src={gitlab_logo}
                alt="Gitlab logo"
                width="20"
                height="20"
              />
            </Navbar.Item>
          </Navbar.Container>
        </Navbar.Menu>
      </Navbar>
    )
  }
}
