import React, { Component } from 'react';
import { FormattedMessage } from "react-intl";
import { Box, Tag } from 'react-bulma-components/full';
import { PostRow } from './PostRow';

export class PostsList extends Component {
  render() {
    return(
      <ul className="PostsList">
        <Tag rounded>
          <strong>
          <FormattedMessage id='postsList.total_unread' />: {
            this.props.posts.filter(post => !post.is_read).length }
          </strong>
        </Tag>
        {this.props.posts.map((post, i) =>
          <Box
            className={post.is_read ?
              "PostRowRead is-marginless" :
              "PostRow is-marginless" }
            key={i}
            onClick={() => this.props.selectPost(post.id)}>

            <PostRow
              post={post}
            />

          </Box>
        )}
      </ul>
    )
  }
}
