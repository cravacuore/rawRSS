import React, { Component } from 'react';
import { Box } from 'react-bulma-components/full';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import { FormattedMessage } from "react-intl";
import { FeedRow } from './FeedRow';


export class FeedList extends Component {

  removeFeedHandle = (e, data) => {
    this.props.removeFeed(data.feed);
  }

  render() {
    return(
      <div className="FeedList">
        <Box className="feedrow-box is-marginless has-text-centered"
            onClick={this.props.listAllPosts}>
          All articles
        </Box>

        {this.props.feeds.map((feed, i) =>
          <ContextMenuTrigger id="rightclick-menu"
            feed={feed} key={i}>
            <Box className="feedrow-box is-marginless" key={i}
              onClick={() => this.props.selectFeed(i)}>

              <FeedRow
                feed       = { feed }
                id         = { feed.id }
                removeFeed = { this.props.removeFeed }
              />

            </Box>
          </ContextMenuTrigger>
        )}

        <ContextMenu id="rightclick-menu">
          {/* TODO */}
          <MenuItem data={{item: 0}} onClick={this.markAsRead}>
            <FormattedMessage id='menu.markAsRead' />
          </MenuItem>
          <MenuItem divider />
          <MenuItem onClick={this.removeFeedHandle}>
            <FormattedMessage id='menu.deleteFeed' />
          </MenuItem>
        </ContextMenu>
      </div>
    )
  }
}
