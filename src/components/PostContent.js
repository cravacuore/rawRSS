import React, { Component } from 'react';
import { FormattedMessage } from "react-intl";
import { Section, Content, Button } from 'react-bulma-components/full';
import read_icon from '../icons/eye-19.svg'

export class PostContent extends Component {
  isYouTubeVideo(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    return regExp.test(url)
  }

  getYouTubeVideoURL(url) {
    // URL types supported
    // http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index
    // http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o
    // http://www.youtube.com/v/0zM3nApSvMg?fs=1&amp;hl=en_US&amp;rel=0
    // http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s
    // http://www.youtube.com/embed/0zM3nApSvMg?rel=0
    // http://www.youtube.com/watch?v=0zM3nApSvMg
    // http://youtu.be/0zM3nApSvMg
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    var result = (match&&match[7].length==11)? match[7] : false;
    return "http://www.youtube.com/embed/" + result
  }

  render() {
    let selectedPost = this.props.selectedPost
    let filtered = this.props.posts.filter(post =>
      post.id === selectedPost)
    let post = (filtered.length > 0 ? filtered.pop() : {})

    return(
      <Section className="PostContent">
        <Content>
          {post.is_read &&
            <Button
              rounded={true}
              onClick={() => this.props.togglePostIsRead(post) }
            >
              <img src={read_icon}
              alt="Toggle is_read icon"
              width="20" height="20" />
              <FormattedMessage id='content.markAsUnread' />
            </Button>
          }
          <h1><a className="title has-text-light"
              href={ post.url } target="_blank">
              { post.title }

          </a></h1>
          { this.isYouTubeVideo(post.url) &&
            <iframe width="560" height="315" src={this.getYouTubeVideoURL(post.url)} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
            </iframe>
          }
          <p className="is-size-6" dangerouslySetInnerHTML=
            {{ __html: post.content }}>
          </p>
        </Content>
      </Section>
    )
  }
}
