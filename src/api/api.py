from flask import Flask
from flask_restful import Api
from flask_cors import CORS

from resources import *

app = Flask(__name__)
api = Api(app)
CORS(app)

api.add_resource(FeedListResource,     '/feeds/',          endpoint='feeds')
api.add_resource(FeedPostListResource, '/feeds/<id>/posts')
api.add_resource(FeedResource,         '/feeds/<id>',      endpoint='feed')
api.add_resource(ParsedFeedResource,   '/feeds/<id>/data', endpoint='posts')
api.add_resource(PostListResource,     '/posts')
api.add_resource(PostResource,         '/posts/<id>')

if __name__ == '__main__':
    app.run(debug=True)
