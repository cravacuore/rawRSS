import api
import json
import sys
import unittest

class TestIntegrations(unittest.TestCase):
    def setUp(self):
        self.app = api.app.test_client()

    def test_get_feeds(self):
        response = self.app.get('/feeds/')
        result   = json.loads(response.get_data().decode(sys.getdefaultencoding()))
        self.assertIsNotNone( result )

from resources import parseFeed
from models import Feed

class TestFeedParser(unittest.TestCase):
    def setUp(self):
        self.url = "http://cravacuore.com.ar/blog/feed.xml"
        self.feed = Feed(url=self.url, title="Test")

    def test_parse_feed(self):
        result  = parseFeed(self.feed)
        feedUrl = result.feed.title_detail.base
        self.assertEqual(feedUrl, self.url)

if __name__ == "__main__":
    unittest.main()
