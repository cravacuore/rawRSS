#!/usr/bin/env python

from sqlalchemy import Column, Integer, String, Text, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

DB_URI = 'sqlite:///database.db'
Base = declarative_base()

class Feed(Base):
    __tablename__ = 'feeds'

    id          = Column(Integer, primary_key=True)
    title       = Column(String(80))
    url         = Column(String(100))
    icon        = Column(String(100))
    last_update = Column(DateTime)
    posts       = relationship('Post', back_populates='feed')


class Post(Base):
    __tablename__ = 'posts'

    id        = Column(Integer, primary_key=True)
    title     = Column(String(80))
    url       = Column(String(80))
    content   = Column(Text)
    author    = Column(String(80), nullable=True)
    is_read   = Column(Boolean, default=False)
    published = Column(DateTime)
    feed_id   = Column(Integer, ForeignKey('feeds.id'))
    feed      = relationship('Feed', back_populates='posts')


if __name__ == "__main__":
    from sqlalchemy import create_engine
    from sqlalchemy_utils import database_exists, create_database

    engine = create_engine(DB_URI)
    if not database_exists(engine.url):
        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)
