from flask_restful import Resource, abort, reqparse, fields, marshal_with
from db import session
from datetime import datetime, timezone, timedelta
import pytz

import feedparser
from models import Feed, Post


feed_fields = {
    'id':          fields.Integer,
    'title':       fields.String,
    'url':         fields.String,
    'icon':        fields.String,
    'last_update': fields.DateTime,
}

parser = reqparse.RequestParser()
parser.add_argument('id')
parser.add_argument('title')
parser.add_argument('url')
parser.add_argument('icon')
parser.add_argument('last_update')


def structToDateTime(struct_time):
    # converts given struct_time type to
    # datetime aware of utc
    return datetime(*struct_time[:6]).replace(tzinfo=None)

def parseFeed(feed):
    url         = feed.url
    data        = feedparser.parse(url)
    data['id']  = feed.id
    entries     = _filterNewEntries(feed, data.entries)
    if feed.title == 'New Feed': # TODO - refactor addFeed (App.js)
        updateFeedTitle(feed, data.feed.title)
    for entry in entries:
        createPostFromParsedEntry(entry, feed)
    return data


def _filterNewEntries(feed, entries_list):
    entries     = entries_list
    if feed is not None:
        last_update = feed.last_update
    if last_update is None:
        # Set old date if last_update not setted
        # 480 weeks = about 10 years ago
        # not elegant but shoud be enough
        last_update = datetime.now().replace(tzinfo=None) - timedelta(weeks=480)

    # filter entries already added since last_update
    entries = [e for e in entries_list if structToDateTime(e.published_parsed) > last_update]
    return entries

def createPostFromParsedEntry(entry, feed):
    publishedDateTime = structToDateTime(entry.published_parsed)
    post = Post(
        title     = entry.title,
        url       = entry.link,
        content   = entry.summary,
        published = publishedDateTime,
        feed_id   = feed.id
    )
    session.add(post)
    session.commit()

def updateFeedLastUpdate(feed):
    feed = session.query(Feed).filter(Feed.id == feed.id).first()
    feed.last_update = datetime.now(timezone.utc)
    session.add(feed)
    session.commit()

def updateFeedTitle(feed, title):
    feed = session.query(Feed).filter(Feed.id == feed.id).first()
    feed.title = title
    session.add(feed)
    session.commit()


class FeedListResource(Resource):
    @marshal_with(feed_fields)
    def get(self):
        feeds = session.query(Feed).all()
        return feeds

    @marshal_with(feed_fields)
    def post(self):
        args = parser.parse_args()
        feed = Feed(
            title       = args['title'],
            url         = args['url'],
            icon        = args['icon'],
            last_update = args['last_update']
        )
        session.add(feed)
        session.commit()
        return feed, 201


class FeedResource(Resource):
    @marshal_with(feed_fields)
    def get(self, id):
        feed = session.query(Feed).filter(Feed.id == id).first()
        if not feed:
            abort(404, message="Feed with id {} doesn't exist".format(id))
        return feed

    @marshal_with(feed_fields)
    def put(self, id):
        args = parser.parse_args()
        feed = session.query(Feed).filter(Feed.id == id).first()
        feed.title       = args['title']
        feed.url         = args['url']
        feed.icon        = args['icon']
        feed.last_update = args['last_update']
        session.add(feed)
        session.commit()
        return feed, 201

    def delete(self, id):
        feed = session.query(Feed).filter(Feed.id == id).first()
        if not feed:
            abort(404, message="Feed with id {} doesn't exist".format(id))
        try:
            posts = session.query(Post).filter(Post.feed_id == id).delete()
            session.delete(feed)
            session.commit()
        except:
            session.rollback()
        return {}, 204


class ParsedFeedResource(Resource):
    def get(self, id):
        feed = session.query(Feed).filter(Feed.id == id).first()
        if not feed:
            abort(404, message="Feed with id {} doesn't exist".format(id))
        parseFeed(feed)
        updateFeedLastUpdate(feed)


post_fields = {
    'id':        fields.Integer,
    'title':     fields.String,
    'url':       fields.String,
    'content':   fields.String,
    'is_read':   fields.Boolean,
    'published': fields.DateTime,
    'feed_id':   fields.Integer,
}

postParser = reqparse.RequestParser()
postParser.add_argument('id')
postParser.add_argument('is_read')

class PostResource(Resource):
    @marshal_with(post_fields)
    def put(self, id):
        args = postParser.parse_args()
        post = session.query(Post).filter(Post.id == id).first()
        post.is_read = args['is_read'] == 'True'
        session.add(post)
        session.commit()
        return post, 201

class FeedPostListResource(Resource):
    @marshal_with(post_fields)
    def get(self, id):
        posts = session.query(Post).filter(Post.feed_id == id).all()
        if not len(posts) > 0:
            abort(404, message="No post found for feed with id {}".format(id))
        return posts


class PostListResource(Resource):
    @marshal_with(post_fields)
    def get(self):
        posts = session.query(Post).all()
        if not len(posts) > 0:
            abort(404, message="No post found")
        return posts

