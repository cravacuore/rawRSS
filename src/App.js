import React, { Component } from 'react';
import { FormattedMessage } from "react-intl";

import { Columns } from 'react-bulma-components/full';
import { Nav } from './components/Nav';
import { FeedList } from './components/FeedList';
import { PostsList } from './components/PostsList';
import { PostContent } from './components/PostContent';

import './react-contextmenu.css';
import './App.css';

const API_BASE_URL = "http://localhost:5000"

export default class App extends Component {
  constructor() {
    super();
    this._initState = {
      feeds:           [],
      feedsData:       [],
      posts:           [],
      isLoaded:        false,
      selectedPost:    0,
      selectedFeed:    0,
      filterReadPosts: false,
    };
    this.state = this._initState;

    // TODO - refactor
    this.stateForReset = {
      feeds:           [],
      feedsData:       [],
      posts:           [],
      isLoaded:        false,
      selectedPost:    0,
      selectedFeed:    0,
    }
  };

  _resetState() {
    this.setState(this.stateForReset);
  }

  setFilterReadPosts(bool) {
    this.setState({ filterReadPosts: bool})
    this.selectFeed( this.state.selectedFeed-1 )
  }

  componentDidMount() {
    this.fetchFeeds();
  }

  fetchFeeds() {
    // Clean old state, needed when refreshing
    this._resetState();
    fetch(API_BASE_URL + "/feeds")
      .then(response => response.json())
      .then(feeds    => { this.updateFeeds(feeds); this.setState({feeds: feeds}) })
      .then(data     => { this.setState({ isLoaded: true }) })
      .catch((e)     => console.log('Error ' + e));
  }

  _updateFeed(feed) {
    fetch(API_BASE_URL + "/feeds/"+ feed.id + "/data")
  }

  updateFeeds(feeds) {
    // This calls api parseFeed for each feed
    // which will populate new posts
    feeds.map(feed => this._updateFeed(feed))
  }

  // Feed Handlers
  addFeed(url, title='New Feed') {
    // TODO - don't admit duplicates
    const formData = new FormData()
    formData.append('url', url)
    formData.append('title', title)

    fetch(API_BASE_URL + "/feeds/", {method: 'POST', body: formData})
      .then(response => response.json())
      .then(data => { this.fetchFeeds() })
      .then(feeds => { this.updateFeeds(feeds) })
      .catch((e) => console.log('Error ' + e));
  }

  removeFeed(feed_id) {
    // Given a feed_id, tries to delete relationated feed
    // from subscriptions
    fetch(API_BASE_URL + "/feeds/" + feed_id, {method: 'DELETE'})
      .then(data => { this.fetchFeeds() })
      .catch((e) => console.log('Error ' + e));
  }

  selectFeed(i) {
    // Sets current selectedFeed and its posts
    let feed  = Object.assign({}, this.state.feeds[i])
    fetch(API_BASE_URL + "/posts")
    .then(response => response.json())
    .then(posts => {
      this.setState({
        selectedFeed: feed.id,
        selectedPost: 0, // resets this one
        posts: posts.filter(post =>
          // filter by feed id
          post.feed_id === feed.id
          // and filter only unread if filterReadPosts = true
          && !(post.is_read && this.state.filterReadPosts))
      })
    })
  }
  //


  // Post Handlers
  postSetIsReadStatus(post_id, status) {
    // status is considered valid only as a string 'True'
    const formData = new FormData()
    formData.append('is_read', status)

    fetch(API_BASE_URL + "/posts/" + post_id, {method: 'PUT', body: formData})
      .then(response => response.json())
      .catch((e) => console.log('Error ' + e));
  }

  togglePostIsRead(post) {
    // Changes is_read status for a given post
    post.is_read ?
      this.postSetIsReadStatus(post.id, 'False') :
      this.postSetIsReadStatus(post.id, 'True');
    this.setState({selectedPost: 0});
    this.setState(prevState => ({
        posts: prevState.posts.map(
          p => (p.id === this.state.selectedPost
            // '' as not read
            ? Object.assign(p, { is_read: '' }) : p)
      )
    }));
  }

  selectPost(i) {
    // Set state selectedPost to clicked post
    this.setState({selectedPost: i});
    // Set is_read true for selectedPost
    this.postSetIsReadStatus(i, 'True');
    this.setState(prevState => ({
        posts: prevState.posts.map(
        post => (post.id === i ? Object.assign(post, { is_read: "True" }) : post)
      )
    }));
  }

  searchPostByTitle(searchString) {
    fetch(API_BASE_URL + "/posts")
    .then(response => response.json())
    .then(posts => {
      this.setState({
        // Filters to any post that includes searchString
        // toLowerCase to be case-insensitive
        posts: posts.filter(post => (post.title).toLowerCase().includes(searchString)),
        selectFeed: 0 // None
      })
    })
  }

  listAllPosts() {
    fetch(API_BASE_URL + "/posts")
    .then(response => response.json())
    .then(posts => {
      this.setState({
        // Get all posts with read posts depending filterReadPosts state
        posts: posts.filter(post => !(post.is_read && this.state.filterReadPosts)),
        selectFeed: 0 // None
      })
    })
  }
  //

  render() {
    let feeds = this.state.feeds;
    let posts = this.state.posts;

    return (
      <div className="App">
        <Nav
          refresh={this.fetchFeeds.bind(this)}
          addFeed={this.addFeed.bind(this)}
          searchPostByTitle={this.searchPostByTitle.bind(this)}
          setFilterReadPosts={this.setFilterReadPosts.bind(this)}
        />

        <Columns>
          <Columns.Column className="FeedListColumn" size={2}>
            <FeedList
              feeds={feeds}
              selectFeed={this.selectFeed.bind(this)}
              removeFeed={this.removeFeed.bind(this)}
              listAllPosts={this.listAllPosts.bind(this)}
            />
          </Columns.Column>

          <Columns.Column className="PostsListColumn">
            { this.state.isLoaded ?
              <PostsList
                posts={posts}
                selectPost={this.selectPost.bind(this)}
              /> :
              <p className="has-text-centered">
                <FormattedMessage id='main.loading' />
              </p>
            }
          </Columns.Column>

          <Columns.Column className="PostContentColumn" size="half">
            <PostContent
              posts={posts}
              selectedPost={this.state.selectedPost}
              togglePostIsRead={this.togglePostIsRead.bind(this)}
            />
          </Columns.Column>
        </Columns>
      </div>
    );
  }
}
