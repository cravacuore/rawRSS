# rawRSS

[![pipeline status](https://gitlab.com/cravacuore/rawRSS/badges/master/pipeline.svg)](https://gitlab.com/cravacuore/rawRSS/commits/master)

***This project now is continued as a pet project of [Molotec](https://molotec.com/) in https://gitlab.com/molotec/rawRSS***

rawRSS aims to be a simple and sleek open-source (and forever free) RSS Reader.

## Features

* Default dark theme
* Intentionally without social networks integrations (maybe as toggleable plugins in the future).
* Formats supported
    * Atom
    * RSS 2.0
    * YouTube channels (coming soon)
    * JSONFeed (not yet)

More info in the [wiki](https://gitlab.com/cravacuore/rawRSS/wikis/home)

## How to install

You need [npm](https://www.npmjs.com/get-npm) and [pip](https://pip.pypa.io/en/stable/installing/) already installed in your system.

First run `install.sh` script.

Then you can use `run.sh` for web version. Or `run-electron.sh` for run as an electron app.
